<?php

include_once("../MPAL/mpal.php");

class MAUTH extends MPAL
{
  public $cookieTokenName = "LoginToken";
  public $cookieIdName = "LoginId";
  public $redirectValid = "/login/restricted.php";
  public $redirectInvalid = "/login/";

  public function RedirectValid()
  {
    header("Location: ".$this->redirectValid);
    die;
  }

  public function RedirectInvalid()
  {
    header("Location: ".$this->redirectInvalid);
    die;
  }

  public function FilterAccess()
  {
    if (!isset($_COOKIE["LoginToken"]) || !isset($_COOKIE["LoginId"]))
    {
      // not logged in
      $this->RedirectInvalid();
    }
    else
    {
      $where = array("id", $_COOKIE["LoginId"]);
      $result = array();
      $count = $this->GetOne("users", "token", $where, $result);
      if ($count == 0)
      {
        // not logged in
        $this->RedirectInvalid();
      }
      else
      {
        if (urldecode($result["token"]) != $_COOKIE["LoginToken"])
        {
          // not logged in
          $this->RedirectInvalid();
        }
        else
        {
          // valid login
        }
      }
    }
  }

  public function CheckCommitLogin()
  {
    if (isset($_GET["commit_login"]))
    {
      $login = $this->SafeInput($_POST["login"]);
      $password = $_POST["password"];

      $result = array();
      $count = $this->GetOne("users", "id,password", array("login", $login), $result);

      if ($count==0)
      {
        echo "login not found.".endl;
      }
      else
      {
        // Failed comparing passwords so must act on it
        if (!password_verify($password, urldecode($result["password"])))
        {
          // TODO: Error pages?
          echo "passwords don't match.".endl;
        }
        // Succeeded authenticating so let's created the cookie
        else
        {
          $token = password_hash(time() + 17, PASSWORD_DEFAULT);
          // We need to store the token in a database to compare it later
          $this->UpdateOne("users", array("token"), array($token), $result["id"]);
          // The user needs a cookie with the token to validate a login
          setcookie($this->cookieTokenName, $token, time() + 3600, "/");
          // We need to know what id the token is about so the user has to store it
          setcookie($this->cookieIdName, $result["id"], time() + 3600, "/");
          // It's nice to redirect them afterwards
          $this->RedirectValid();
        }
      }
    }
  }

  public function CheckCommitLogout()
  {
    if (isset($_GET["commit_logout"]))
    {
      // TODO: should we reset the token in the database for safety?
      setcookie($this->cookieTokenName, "", time() - 1, "/");

      header("Location: /login/");
    }
  }

  public function CheckCommitRegister()
  {
    if (isset($_GET["commit_register"]))
    {
      // Since we have the following fields as UNIQUE: login, email
      // We have to check for them manually in order to notify the
      // user. TODO: Is this something we can have SQL pass directly?
      $r = array();
      $count = $this->getOne("users", "email", array("login", $_POST["login"]), $r);
      if ($count>0)
      {
        $emailDecoded = urldecode($r["email"]);
        $emailParts = explode("@", $emailDecoded);
        $emailSecret = substr($emailParts[0], 0, 2)."...@".$emailParts[1];
        // TODO Error pages or a different approach?
        echo "Sorry. ): This login name already exists with another email: ".$emailSecret;

        die;
      }
      $r = array();
      $count = $this->getOne("users", "login", array("email", $_POST["email"]), $r);
      if ($count>0)
      {
        $loginSecret = substr($r["login"], 0, 2)."..."
          .substr($r["login"], strlen($r["login"])-2, strlen($r["login"]));
        // TODO Error pages or a different approach?
        echo "Sorry. ): This email already exists with another login: ".$loginSecret;
        die;
      }

      // We should just use default hash function which has it's own salt
      $passwordHashed = password_hash($_POST["password"], PASSWORD_DEFAULT);

      $er = $this->InsertOne("users", array("login","email","password", "token"),
        array($_POST["login"], $_POST["email"], $passwordHashed, ""));

      if ($er->code > 0)
      {
        echo $er->messages[0].endl;
        die;
      }
      else
      {
        echo "User register successful.".endl;
      }
    }
  }
}

?>
